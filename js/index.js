
$(function () {
	$("[data-toggle='tooltip']").tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
		interval: 2000
	});
	$('#contacto').on('show.bs.modal',function(e){
		console.log("El modal se esta mostrando");
		$('#contactoboton').removeClass('btn-outline-success');
		$('#contactoboton').addClass('btn-success');
		$('#contactoboton').prop('enabled','true');
	});
	$('#contacto').on('shown.bs.modal',function(e){
		console.log("El modal se esta mostrando");
	});
	$('#contacto').on('hide.bs.modal',function(e){
		console.log("El modal se esta ocultando");
		$('#contactoboton').removeClass('btn-success');
		$('#contactoboton').addClass('btn-outline-success');
	});
	$('#contacto').on('hidden.bs.modal',function(e){
		console.log("El modal se esta ocultando");
	});
})
